package at.cic.task;

import com.google.common.base.Predicate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


import static com.google.common.base.Predicates.or;
import static springfox.documentation.builders.PathSelectors.regex;

/**
 * Created by q387275 on 25.01.2017.
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("group-api")
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(paths()) // and by paths
                .build()
                .apiInfo(apiInfo());
    }

    //Here is an example where we select any api that matches one of these paths
    private Predicate<String> paths() {
        return or(regex("/home.*"));
    }

    private ApiInfo apiInfo() {
        ApiInfo apiInfo = new ApiInfo(
                "Movies API",
                "API description",
                "API TOS",
                "Terms of service",
                "tbd@tbd.com",
                "License of API",
                "API license URL");
        return apiInfo;
    }




}