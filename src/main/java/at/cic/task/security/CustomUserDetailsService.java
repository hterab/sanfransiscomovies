package at.cic.task.security;

import at.cic.task.security.impl.User;
import at.cic.task.security.loginattempt.LoginAttemptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;

/**
 * Created by q387275 on 25.01.2017.
 */
@Service
public class CustomUserDetailsService implements UserDetailsService {


    @Autowired
    private LoginAttemptService loginAttemptService;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return null;
    }


    private final static class UserRepositoryUserDetails extends User implements UserDetails {

        private static final long serialVersionUID = 1L;


        private LoginAttemptService loginAttemptService;

        private UserRepositoryUserDetails(User user, LoginAttemptService loginAttemptService) {
            super(user);
            this.loginAttemptService = loginAttemptService;

        }


        @Override
        public Collection<? extends GrantedAuthority> getAuthorities() {
            return getRoles();
        }

        @Override
        public String getUsername() {
            return getLogin();
        }

        @Override
        public boolean isAccountNonExpired() {
            return true;
        }

        @Override
        public boolean isAccountNonLocked() {

            HttpServletRequest request =
                    ((ServletRequestAttributes) RequestContextHolder.
                            currentRequestAttributes()).
                            getRequest();

            String remoteAddr = request.getRemoteAddr();

            if (request.getRemoteAddr() != null){
                return !loginAttemptService.isBlocked(remoteAddr);
            }else{
                return true; //on default: account is not locked
            }


        }

        @Override
        public boolean isCredentialsNonExpired() {
            return true;
        }

        @Override
        public boolean isEnabled() {
            return true;
        }

    }

}
