package at.cic.task.security.loginattempt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;

/**
 * Created by q387275 on 25.01.2017.
 */
@Component
public class AuthenticationFailureListener
        implements ApplicationListener<AuthenticationFailureBadCredentialsEvent> {

    @Autowired
    private LoginAttemptService loginAttemptService;

    public void onApplicationEvent(AuthenticationFailureBadCredentialsEvent e) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (auth != null) {
            WebAuthenticationDetails details = (WebAuthenticationDetails) auth.getDetails();
            String ipAddress = details.getRemoteAddress();
            loginAttemptService.loginFailed(ipAddress);
        }
    }
}