package at.cic.task.security.loginattempt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Component;

/**
 * Created by q387275 on 25.01.2017.
 */
@Component
public class AuthenticationSuccessEventListener
        implements ApplicationListener<AuthenticationSuccessEvent> {

    @Autowired
    private LoginAttemptService loginAttemptService;

    public void onApplicationEvent(AuthenticationSuccessEvent e) {
        //WebAuthenticationDetails auth = (WebAuthenticationDetails)
        //      e.getAuthentication().getDetails();

        //  java.util.LinkedHashMap a = (java.util.LinkedHashMap)e.getAuthentication().getDetails();

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (auth != null){
            WebAuthenticationDetails details = (WebAuthenticationDetails) auth.getDetails();
            String ipAddress = details.getRemoteAddress();
            loginAttemptService.loginSucceeded(ipAddress);
        }


    }


}