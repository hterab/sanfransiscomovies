package at.cic.task.controllers;

import at.cic.task.dtos.MovieDto;
import at.cic.task.services.MovieService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by q387275 on 25.01.2017.
 */



@RestController
@RequestMapping("/movie")
public class MovieController {

    private static final Logger LOG = LogManager.getLogger(MovieController.class);

    @Autowired
    private MovieService movieService;

    @RequestMapping(value="/",  method = RequestMethod.GET)
    public String home() {
        return "Main Home - " + new Date();
    }


    @RequestMapping(value="/all",  method = RequestMethod.GET, headers = "Accept=application/json",
            produces = "application/json")
    public List<MovieDto> callTest(){
        return movieService.loadAllMovies();
    }



    @RequestMapping(value="/find/{title}",
            method = RequestMethod.GET,
            headers = "Accept=application/json",
            produces = "application/json")
    public ResponseEntity<List<MovieDto>> searchMovieByTitle(@PathVariable("title") String title) {

        List<MovieDto> movies = movieService.searchMovie(title);
        if(movies != null)
            return new ResponseEntity<List<MovieDto>>(movies, HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        /*
        if (movies != null && movies.length > 0) {
            for (MovieDto movieDto : movies) {
                if (movieDto.getTitle().equals(title)) {
                    return new ResponseEntity<MovieDto>(movieDto, HttpStatus.OK);
                }
            }
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        */
    }
}
