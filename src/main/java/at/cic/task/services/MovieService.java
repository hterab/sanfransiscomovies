package at.cic.task.services;

import at.cic.task.controllers.MovieController;
import at.cic.task.dtos.MovieDto;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Terab on 25.01.2017.
 */
@Service("movieService")
public class MovieService {
    private static final Logger LOG = LogManager.getLogger(MovieService.class);

    MovieDto[] movies;
    public  MovieService(){

		RestTemplate restTemplate = new RestTemplate();
		movies = restTemplate.getForObject("https://data.sfgov.org/resource/wwmu-gmzc.json?$limit=2000", MovieDto[].class );
		if(movies != null && movies.length > 0){
			System.out.println("Size: " + movies.length);
		}else{
			System.out.println("EMPTY");
		}

		/*
        movies = new MovieDto[5];
        movies[0] = new MovieDto("A1", 2010);
        movies[1] = new MovieDto("A2", 2011);
        movies[2] = new MovieDto("A3", 2012);
        movies[3] = new MovieDto("A4", 2013);
        movies[4] = new MovieDto("A5", 2014);
        */
    }


    public List<MovieDto> loadAllMovies(){
        return Arrays.asList(movies);
    }

    public  List<MovieDto>  searchMovie(String title){

        List<MovieDto> foundMovies = new ArrayList<MovieDto>();
        if (movies != null && movies.length > 0) {

            for (MovieDto movieDto : movies) {
                if (movieDto.getTitle().equals(title)) {
                  foundMovies.add(movieDto);
                }
            }
            return foundMovies;
        }
        return null;

    }
}
