import at.cic.task.Application;
import at.cic.task.controllers.MovieController;
import at.cic.task.dtos.MovieDto;
import at.cic.task.services.MovieService;
import com.google.gson.Gson;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Arrays;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by q387275 on 25.01.2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = Application.class)
public class MovieRestTest {

    @Autowired
    WebApplicationContext context;

    @Autowired
    private FilterChainProxy springSecurityFilterChain;

    @Mock
    private MovieService movieServiceMock;

    @InjectMocks
    MovieController controller;





    private MockMvc mvc;




    private String SERVER_URL ="http://localhost:8080/movie";

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mvc = MockMvcBuilders.webAppContextSetup(context)
                .addFilter(springSecurityFilterChain).build();
        
    }


    @Test
    public void home() throws Exception {
        // @formatter:off
        mvc.perform(get("/")
                .accept(MediaType.TEXT_PLAIN))
                .andExpect(status().isOk())
                .andExpect(content().string("home"));
        // @formatter:on
    }


    @Test
    public void testAllMovies() throws Exception {

        Gson gson = new Gson();

        // @formatter:off
        mvc.perform(get(SERVER_URL + "/all")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(5)));
        //.andExpect(content().string("home"));
        // @formatter:on
    }


    @Test
    public void testFindMovieByTitle_Ok() throws Exception {

        Gson gson = new Gson();

        MovieDto m = new MovieDto("A2", "");

        // @formatter:off
        MvcResult result = mvc.perform(get(SERVER_URL + "/find/A2")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(gson.toJson(m)))
                .andReturn();

    }

    @Test
    public void testFindMovieByTitle_NotFound() throws Exception {
        mvc.perform(get(SERVER_URL + "/find/NOTFOUNDMOVIE"))
                .andExpect(status().isNotFound());
    }



    @Test
    public void findAll_TodosFound_ShouldReturnFoundTodoEntries() throws Exception {
        MovieDto m1 = new MovieDto("A1", "2010");
        MovieDto m2 = new MovieDto("A2", "2011");
        when(movieServiceMock.loadAllMovies()).thenReturn(Arrays.asList(m1, m2));


        mvc.perform(get(SERVER_URL + "/all")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(5)))
                .andExpect(jsonPath("$[0].title", is("A1")))
                .andExpect(jsonPath("$[1].title", is("A2"))
                );

     //   verify(movieServiceMock, times(1)).loadAllMovies();

        movieServiceMock.loadAllMovies();
        verify(movieServiceMock, times(1)).loadAllMovies();
        verifyNoMoreInteractions(movieServiceMock);

    }
}
